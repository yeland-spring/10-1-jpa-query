package com.twuc.webApp.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    List<Product> findAllByProductLine_TextDescriptionContains(String content);

    List<Product> findAllByQuantityInStockBetween(short min, short max);

    List<Product> findAllByQuantityInStockBetweenOrderByProductCode(short min, short max);

    List<Product> findTop3ByQuantityInStockBetweenOrderByProductCode(short min, short max);

    Page<Product> findAllByQuantityInStockBetween(short min, short max, Pageable pageable);

    // --end-->
}